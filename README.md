This is a project to analyse the text corpus parameters of Malayalam

- Type-Token Ratio (TTR)
- Type_token Growth Rate (TTGR)
- Moving Average Type Token Ratio (MATTR)


### Text Corpus

The word.txt contains words from the Malayalam Wikipedia Corpus from https://gitlab.com/smc/corpus, retrieved on January 1, 2019. It is cleaned up to remove punctuations and foreign language scripts and is unicode normalized.

### Computing Type Count

- To compute the number of types for any set of tokens, check the script ml_ttr.sh
- Set the indices for the range of tokens over which you need to determine the type count.

### Type and Token Counts

- `typetoken.tsv` contains the type count for different token counts, starting from the beginning of `words.txt`
- `ttr.tsv` contains the token count for a window length of 500, moved smoothly along `words.txt`

### Ipython notebook  `ttrplots.ipynb`

- Generates various plots used in TSD-2020 paper Quantitative Analysis of the Morphological Complexity of Malayalam Language
- The values from secondary sources (Kettunen, 2014 https://www.tandfonline.com/doi/abs/10.1080/09296174.2014.911506 and Bharadwaja Kumar et al., 2007 http://library.isical.ac.in:8080/jspui/bitstream/10263/2306/1/statiscal%20analysis%20of%20telugu%20text%20corpora.pdf) are hard-coded in the script.


### Papers citing this work

1. [*Optimal Word Segmentation for Neural Machine Translation into Dravidian Languages*](https://aclanthology.org/2021.wat-1.21.pdf); Prajit Dhar, Arianna Bisazza, Gertjan van Noord; Proceedings of the 8th Workshop on Asian Translation (WAT2021)

2. [*Pre-trained Word Embeddings for Malayalam Language: A Review*](https://ieeexplore.ieee.org/abstract/document/9396042); K Reji Rahmath; P C Reghu Raj; P C Rafeeque; 2021 International Conference on Artificial Intelligence and Smart Systems (ICAIS)

### To cite this work

Read the paper published in 23rd International Conference on Text, Speech and Dialogue [here](https://link.springer.com/chapter/10.1007/978-3-030-58323-1_7)

```
@InProceedings{10.1007/978-3-030-58323-1_7,
author="Manohar, Kavya
and Jayan, A. R.
and Rajan, Rajeev",
editor="Sojka, Petr
and Kope{\v{c}}ek, Ivan
and Pala, Karel
and Hor{\'a}k, Ale{\v{s}}",
title="Quantitative Analysis of the Morphological Complexity of Malayalam Language",
booktitle="Text, Speech, and Dialogue",
year="2020",
publisher="Springer International Publishing",
address="Cham",
pages="71--78",
abstract="This paper presents a quantitative analysis on the morphological complexity of Malayalam language. Malayalam is a Dravidian language spoken in India, predominantly in the state of Kerala with about 38 million native speakers. Malayalam words undergo inflections, derivations and compounding leading to an infinitely extending lexicon. In this work, morphological complexity of Malayalam is quantitatively analyzed on a text corpus containing 8 million words. The analysis is based on the parameters type-token growth rate (TTGR), type-token ratio (TTR) and moving average type-token ratio (MATTR). The values of the parameters obtained in the current study is compared to that of the values of other morphologically complex languages.",
isbn="978-3-030-58323-1"
}
```